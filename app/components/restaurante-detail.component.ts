import { Component, OnInit } from "angular2/core";
import { RouteParams, Router } from "angular2/router";
import { RestauranteService } from "../services/restaurante.service";
import { Restaurante } from "../model/restaurante";

@Component({
    selector: "restaurante-detail",
    templateUrl: "app/view/restaurante-detail.html",
    providers: [RestauranteService]
})

export class RestauranteDetailComponent implements OnInit {
    public parametro
    public restaurante: Restaurante[];

    constructor(
        private _restauranteService: RestauranteService,
        private _routeParams: RouteParams,
        private _router: Router
    ) {

    }

    ngOnInit() {
        this.parametro = this._routeParams.get("id");
        this.getRestaurante();
    }

    getRestaurante() {
        let id = this._routeParams.get("id");
        let random = this._routeParams.get("random");

        this._restauranteService.getRestaurante(id, random)
            .subscribe(
            response => {
                this.restaurante = response.data;
                let status = response.status;
                if (status !== "success") {
                    this._router.navigate(['Home']);
                    //alert("Error en el server");
                }
            },
            error => {
                let errorMessage = <any>error;

                if (errorMessage !== null) {
                    console.log(errorMessage);
                    alert("Error en la peticion");
                }
            }
            )
    }

}